
#include "stm32f1xx_hal.h"

#define DELAY 100

void alloff(int invert = 0) {
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)(GPIO_PinState) ((int)GPIO_PIN_SET ^ invert)) );
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
}

void left(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
}

void right(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
}

void fillright(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
}

void fillleft(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
}

void fillrightleft(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
}

void fillleftright(int invert = 0) {
	alloff(invert);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_SET ^ invert));
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
  HAL_Delay(DELAY);
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, (GPIO_PinState) ((int)GPIO_PIN_RESET ^ invert));
}

void disco() {
	left();
	left();
	left();
	fillleft();
	right();
	right();
	right();
	fillright();

	fillleft();
	fillrightleft();
	left(1);
	left(1);
	fillleft(1);
	right(1);
	right(1);
	fillright(1);
	left(1);
	left(1);
	fillleftright(1);

	fillleft();
	fillright();

	left();
	fillleft();
	right();
	fillright();
}
