/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : can_defs.h
 * - Date       : 2. mar. 2020
 * - Description:
 * -
 * -------------------------------------*/

#ifndef STATUS_H_
#define STATUS_H_

#include <stdint.h>

// Defines
//---------------------------------

enum class STATUS_SOURCE {
			UNDEFINED = 0,
			PRESSURE_SENSORS,
			BMP280_1,
			BMP280_2,
			BMP280_3,
			SENSOR_LAYER_ADC,
			GPS
};

struct Status {
	STATUS_SOURCE source;
	uint16_t statusCode;

	Status() {
		source = STATUS_SOURCE::UNDEFINED;
		statusCode = 0;
	}

	Status(STATUS_SOURCE source, uint16_t statusCode) {
		this->source = source;
		this->statusCode = statusCode;
	}

	Status(uint8_t *data) {
		((uint8_t*)(&this->source))[0] = data[0];
		((uint8_t*)(&this->source))[1] = data[1];
		((uint8_t*)(&this->statusCode))[0] = data[2];
		((uint8_t*)(&this->statusCode))[1] = data[3];
	}
};



#endif /* STATUS_H_ */
