/*
* CircularBuffer.h
*
* Created: 08.12.2013 16:14:47
* Author: Jonas
*/


#ifndef __CIRCULARBUFFER_H__
#define __CIRCULARBUFFER_H__

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

typedef void (*CircularBufferCallback)(void);

template <class T>
class CircularBuffer {
public:
	CircularBuffer(uint8_t size);
	~CircularBuffer();

	bool put(T d);
	T get();
	T getNondestructiv(uint16_t offset);

	void removeLastByte();
	void clear();

	uint16_t getFreeSpace();
	uint16_t getUsedSpace();
	bool isEmpty();
	bool isFull();

	bool error();
	void registerPutCallback(CircularBufferCallback callback);

private:
	T *buffer;
	uint16_t rp, wp;
	uint16_t overflowMask;
	uint16_t size;
	bool full;
	bool error_;
	CircularBufferCallback putCallback;
};


template <class T>
CircularBuffer<T>::CircularBuffer(uint8_t size)
{
	putCallback = nullptr;

	size = (1 << size);
	overflowMask = this->size - 1;

	buffer = (T*) malloc(sizeof(T) * this->size);

	if(buffer == NULL)
		error_ = true;

  rp = 0;
  wp = 0;
	full = 0;
}

template <class T>
CircularBuffer<T>::~CircularBuffer() {
	if(buffer != NULL)
		free(buffer);
}

template <class T>
bool CircularBuffer<T>::put(T c)
{
	if(isFull())
		return false;

	*(buffer + wp) = c;
	wp++;
  wp &= overflowMask;

	full = (wp == rp);

	if(putCallback)
		putCallback();

	return true;
}

template <class T>
T CircularBuffer<T>::get() {
	T c = *(buffer + rp);
	rp++;
	rp &= overflowMask;

	full = false;

	return c;
}

template <class T>
T CircularBuffer<T>::getNondestructiv(uint16_t offset) {
  return *(buffer + ((rp+offset) & overflowMask));
}

template <class T>
void CircularBuffer<T>::removeLastByte() {
	rp++;
	rp &= this->overflowMask;
}

template <class T>
void CircularBuffer<T>::clear() {
  rp = wp;
}

template <class T>
uint16_t CircularBuffer<T>::getUsedSpace() {
	if(full)
		return size;

  if(wp < rp)
		return size - (rp - wp);

	return wp - rp;
}

template <class T>
uint16_t CircularBuffer<T>::getFreeSpace() {
	if(full)
		return 0;

	if(wp < rp)
		return rp - wp;

  return size - (wp - rp);
}


template <class T>
bool CircularBuffer<T>::isEmpty() {
  return rp == wp && !full;
}

template <class T>
bool CircularBuffer<T>::isFull() {
	return full;
}

template <class T>
bool CircularBuffer<T>::error() {
	return error_;
}

template <class T>
void CircularBuffer<T>::registerPutCallback(CircularBufferCallback callback) {
	putCallback = callback;
}


#endif //__CIRCULARBUFFER_H__
