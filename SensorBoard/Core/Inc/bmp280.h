/*
 * bmp280.h
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */

#ifndef INC_BMP280_H_
#define INC_BMP280_H_

#include "stm32f1xx_hal.h"
#include "Drivers/bmp280_defs.h"
#include "Drivers/bmp280.h"
#include <status.h>

#define SENSOR_PORT GPIOB
#define SENSOR_1_CS GPIO_PIN_3
#define SENSOR_2_CS GPIO_PIN_4
#define SENSOR_3_CS GPIO_PIN_5


Status BMP280_Init(SPI_HandleTypeDef *_hspi);

Status BMP280_GetPressureAndTemperature(int sensorId, double *pressure, double *temperature);


#endif /* INC_BMP280_H_ */
