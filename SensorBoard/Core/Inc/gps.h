/*
 * gps.h
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */

#ifndef INC_GPS_H_
#define INC_GPS_H_

#include "stm32f1xx_hal.h"

int GPS_Init(UART_HandleTypeDef *_huart);
bool GPS_Fix();
bool GPS_GetReading(float *latitude, float *longitude, float *altitude, float *speed, float *angle);


#endif /* INC_GPS_H_ */
