/*
 * can.h
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */

#ifndef INC_DRIVERS_CAN_H_
#define INC_DRIVERS_CAN_H_

#include "stm32f1xx_hal.h"
#include "CircularBuffer.h"
#include "can_defs.h"
#include "status.h"


struct CAN_TxMessage {
	CAN_TxHeaderTypeDef header;
	uint8_t data[8];
};


int CAN_Init();
void CAN_Send(CAN_TxMessage *message);
void CAN_Send(CAN_ID id, uint8_t *data, uint8_t length);
void CAN_Send(CAN_ID id, uint8_t data);
void CAN_Send(CAN_ID id, uint16_t data);
void CAN_Send(CAN_ID id, uint32_t data);
void CAN_Send(CAN_ID id, uint64_t data);
void CAN_Send(CAN_ID id, float data);
void CAN_Send(CAN_ID id, double data);
void CAN_Send(Status data);

extern "C" {
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan);
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan);
}


#endif /* INC_DRIVERS_CAN_H_ */
