/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : sensorFusion.h              
 * - Date       : Mar 2, 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

#ifndef __INC_SENSORFUSION_H__
#define __INC_SENSORFUSION_H__

// Includes
//---------------------------------
#include "status.h"

// Prototype Declarations
//---------------------------------
Status getFusedPressureAndTemperature(double *pressure, double *temperature);

#endif /* INC_SENSORFUSION_H_ */
