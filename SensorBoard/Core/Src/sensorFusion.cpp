/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : sensorFusion.cpp              
 * - Date       : Mar 2, 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

// Includes
//---------------------------------
#include "sensorFusion.h" 
#include "bmp280.h"

Status getFusedPressureAndTemperature(double *pressure, double *temperature) {
	int i;
	double pressures[3], temperatures[3];
	Status status;

	// Get all the sensor data and return the error code if there is an error
	for(i=0; i<3; i++) {
    status = BMP280_GetPressureAndTemperature(i, pressures+i, temperatures+i);
		if(status.statusCode != BMP280_OK)
			return status;
	}

	// TODO the sensor fusion


	return Status(STATUS_SOURCE::PRESSURE_SENSORS, SUCCESS);
}
