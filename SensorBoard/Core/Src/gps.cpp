/*
 * gps.c
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */

#include "gps.h"
#include "Drivers/Adafruit_GPS.h"

UART_HandleTypeDef *huart;
Adafruit_GPS *gps;

int GPS_Init(UART_HandleTypeDef *_huart) {
	huart = _huart;

	gps = Adafruit_GPS::getInstance();

	gps->begin(huart);

	// turn on RMC (recommended minimum) and GGA (fix data) including altitude
	gps->sendCommand(PMTK_SET_NMEA_OUTPUT_RMCGGA);
	// Set the update rate
	gps->sendCommand(PMTK_SET_NMEA_UPDATE_1HZ); // 1 Hz update rate
	// Request updates on antenna status, comment out to keep quiet
	gps->sendCommand(PGCMD_ANTENNA);

	HAL_Delay(500);
	// set baud rate of GPS module to 115200
	gps->sendCommand(PMTK_SET_BAUD_115200);
	HAL_Delay(100);
	// set baud rate of uC to 115200
	HAL_UART_DeInit(huart);
	huart->Init.BaudRate = 115200;
	if (HAL_UART_Init(huart) != HAL_OK)
		return ERROR;

	gps->begin(huart);

	return SUCCESS;
}

bool GPS_Fix() {
	return gps->fix;
}

bool GPS_GetReading(float *latitude, float *longitude, float *altitude, float *speed, float *angle) {
	*latitude = gps->latitude;
	*longitude = gps->longitude;
	*altitude = gps->altitude;
	*speed = gps->speed;
	*angle = gps->angle;
	return gps->newNMEAreceived();
}



