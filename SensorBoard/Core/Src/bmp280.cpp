/*
 * bmp280.cpp
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */


#include <bmp280.h>
#include <buffer.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>


bmp280_dev bmp[3];

const uint16_t SENSOR_CS[] = {SENSOR_1_CS, SENSOR_2_CS, SENSOR_3_CS};

uint32_t Init(int sensorId);
void handle_rslt(const char api_name[], int8_t rslt);
int8_t spi_reg_write(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);
int8_t spi_reg_read(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length);

SPI_HandleTypeDef *hspi;

Status BMP280_Init(SPI_HandleTypeDef *_hspi) {
	int8_t rslt;

	hspi = _hspi;

	rslt = Init(0);
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_1, rslt);

	Init(1);
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_2, rslt);

	Init(2);
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_3, rslt);

	return Status(STATUS_SOURCE::BMP280_3, BMP280_OK);
}

Status BMP280_GetPressureAndTemperature(int sensorId, double *pressure, double *temperature) {
  struct bmp280_uncomp_data ucomp_data;

	// Reading the raw data from sensor
	int rslt = bmp280_get_uncomp_data(&ucomp_data, &(bmp[sensorId]));
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_1, rslt);

	rslt  = bmp280_get_comp_pres_double(pressure, ucomp_data.uncomp_press, &(bmp[sensorId]));
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_2, rslt);

	rslt  = bmp280_get_comp_temp_double(temperature, ucomp_data.uncomp_temp, &(bmp[sensorId]));
	if(rslt != BMP280_OK) return Status(STATUS_SOURCE::BMP280_3, rslt);

	return Status(STATUS_SOURCE::BMP280_1, BMP280_OK);
}


uint32_t Init(int sensorId) {
	// Init barometer
  int8_t rslt;
  struct bmp280_config conf;

  // Map the delay function pointer with the function responsible for implementing the delay
  bmp[sensorId].delay_ms = HAL_Delay;

  bmp[sensorId].dev_id = SENSOR_CS[sensorId];
  bmp[sensorId].read = spi_reg_read;
  bmp[sensorId].write = spi_reg_write;
  bmp[sensorId].intf = BMP280_SPI_INTF;

  rslt = bmp280_init(&(bmp[sensorId]));
  if(rslt != BMP280_OK) return rslt;
  //handle_rslt(" bmp280_init status", rslt);

  // Always read the current settings before writing, especially when
  // all the configuration is not modified
  rslt = bmp280_get_config(&conf, &(bmp[sensorId]));
  if(rslt != BMP280_OK) return rslt;
  //handle_rslt(" bmp280_get_config status", rslt);

  // configuring the temperature oversampling, filter coefficient and output data rate
  // Overwrite the desired settings
  conf.filter = BMP280_FILTER_COEFF_2;

  // Pressure oversampling set at 4x
  conf.os_pres = BMP280_OS_4X;

  // Temperature oversampling
  conf.os_temp = BMP280_OS_4X;

  // Setting the output data rate as 1HZ(1000ms)
  conf.odr = BMP280_ODR_1000_MS;
  rslt = bmp280_set_config(&conf, &(bmp[sensorId]));
  if(rslt != BMP280_OK) return rslt;
  //handle_rslt(" bmp280_set_config status", rslt);

  // Always set the power mode after setting the configuration
  rslt = bmp280_set_power_mode(BMP280_NORMAL_MODE, &(bmp[sensorId]));
  if(rslt != BMP280_OK) return rslt;
  //handle_rslt(" bmp280_set_power_mode status", rslt);

  return BMP280_OK;
}


///*!
// *  @brief Function for writing the sensor's registers through SPI bus.
// *
// *  @param[in] cs           : Chip select to enable the sensor.
// *  @param[in] reg_addr     : Register address.
// *  @param[in] reg_data : Pointer to the data buffer whose data has to be written.
// *  @param[in] length       : No of bytes to write.
// *
// *  @return Status of execution
// *  @retval 0 -> Success
// *  @retval >0 -> Failure Info
// *
// */

int8_t spi_reg_write(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length)
{
	// select the chip
	HAL_GPIO_WritePin(GPIOB, cs, GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi, &reg_addr, 1, 10000);
	HAL_SPI_Transmit(hspi, reg_data, length, 10000);

	HAL_GPIO_WritePin(GPIOB, cs, GPIO_PIN_SET);
    return 0;
}

/*!
 *  @brief Function for reading the sensor's registers through SPI bus.
 *
 *  @param[in] cs       : Chip select to enable the sensor.
 *  @param[in] reg_addr : Register address.
 *  @param[out] reg_data    : Pointer to the data buffer to store the read data.
 *  @param[in] length   : No of bytes to read.
 *
 *  @return Status of execution
 *  @retval 0 -> Success
 *  @retval >0 -> Failure Info
 *
 */
int8_t spi_reg_read(uint8_t cs, uint8_t reg_addr, uint8_t *reg_data, uint16_t length)
{
	HAL_GPIO_WritePin(GPIOB, cs, GPIO_PIN_RESET);

	HAL_SPI_Transmit(hspi, &reg_addr, 1, 10000);
	HAL_SPI_Receive(hspi, reg_data, length, 10000);

	HAL_GPIO_WritePin(GPIOB, cs, GPIO_PIN_SET);

  return 0;
}

/*!
 *  @brief Prints the execution status of the APIs.
 *
 *  @param[in] api_name : name of the API whose execution status has to be printed.
 *  @param[in] rslt     : error code returned by the API whose execution status has to be printed.
 *
 *  @return void.
 */
void handle_rslt(const char api_name[], int8_t rslt)
{
	/*snprintf(buffer, sizeof(buffer), "%s\r\n", api_name);
	if (rslt != BMP280_OK)
	{
		if (rslt == BMP280_E_NULL_PTR)
		{
			snprintf(buffer, sizeof(buffer), "Error [%d] : Null pointer error\r\n", rslt);
		}
		else if (rslt == BMP280_E_COMM_FAIL)
		{
			snprintf(buffer, sizeof(buffer), "Error [%d] : Bus communication failed\r\n", rslt);
		}
		else if (rslt == BMP280_E_IMPLAUS_TEMP)
		{
			snprintf(buffer, sizeof(buffer), "Error [%d] : Invalid Temperature\r\n", rslt);
		}
		else if (rslt == BMP280_E_DEV_NOT_FOUND)
		{
			snprintf(buffer, sizeof(buffer), "Error [%d] : Device not found\r\n", rslt);
		}
		else
		{
			//For more error codes refer "*_defs.h"
			snprintf(buffer, sizeof(buffer), "Error [%d] : Unknown error code\r\n", rslt);
		}
	}*/
	//Send_Uart(buffer);
}
