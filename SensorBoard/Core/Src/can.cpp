/*
 * can.cpp
 *
 *  Created on: Feb 29, 2020
 *      Author: jonas
 */

#include <can.h>
#include <status.h>


extern CAN_HandleTypeDef hcan;
uint32_t TxMailbox;
// CAN_TxMessage is defined in the can.h
CircularBuffer<CAN_TxMessage> sendBuffer(5);	// Size = 2^5 * sizeof(CAN_TxMessage)

void CAN_Transmit();
void CAN_SendBufferPutCallback();
void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data);


int CAN_Init() {
	if(sendBuffer.error())
		return ERROR;

	sendBuffer.registerPutCallback(CAN_SendBufferPutCallback);
	return SUCCESS;
}

void CAN_Send(CAN_TxMessage message) {
	// put() returns false if the buffer is full, in this case just try again
	while(!sendBuffer.put(message));
}

void CAN_Send(CAN_ID id, uint8_t *data, uint8_t length) {
	int i;
	CAN_TxMessage message;

	// set the header data
	message.header.StdId = (uint32_t) id;
	message.header.DLC = length;

	// copy the data
	for(i=0; i<length; ++i)
		message.data[i] = data[i];

	// put the message in the send buffer
	// put() returns false if the buffer is full, in this case just try again
	while(!sendBuffer.put(message));
}

// some overloads to make sending more easy
void CAN_Send(CAN_ID id, uint8_t data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(uint8_t));
}

void CAN_Send(CAN_ID id, uint16_t data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(uint16_t));
}

void CAN_Send(CAN_ID id, uint32_t data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(uint32_t));
}

void CAN_Send(CAN_ID id, uint64_t data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(uint64_t));
}

void CAN_Send(CAN_ID id, float data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(float));
}

void CAN_Send(CAN_ID id, double data) {
	CAN_Send(id, (uint8_t*) &data, sizeof(double));
}

void CAN_Send(Status data) {
	CAN_Send(CAN_ID::STATUS, (uint8_t*) &data, sizeof(CAN_Status));
}


void CAN_Transmit() {
	if(!sendBuffer.isEmpty()) {
		// get a message from the send buffer and send it
		CAN_TxMessage message = sendBuffer.get();
		if(HAL_CAN_AddTxMessage(&hcan, &(message.header), message.data, &TxMailbox) != HAL_OK) {
			// TODO error
		}
	}
}


void CAN_SendBufferPutCallback() {
	// This is called when a message is put into the send buffer
	// If a mailbox is free send this message
	if(HAL_CAN_GetTxMailboxesFreeLevel(&hcan) > 0)
		CAN_Transmit();
}


extern "C" {

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
	CAN_Transmit();
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
	CAN_Transmit();
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
	CAN_Transmit();
}


void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &pHeader, data);
	//CAN_Message_Received(&pHeader, data);
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &pHeader, data);
	//CAN_Message_Received(&pHeader, data);
}

}


void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data) {
	//snprintf(buffer, sizeof(buffer), "%d %x %x\r\n", (int) pHeader->DLC, *((unsigned int*) (data+4)), *((unsigned int*) data));
	//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

	//if(pHeader->StdId == 1) {
		//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
		//HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);

		//if(sdCardAvailable) {
		//	f_puts("click\n", &datacsv);
		//}
	//}
	//if(pHeader->StdId == 2) {
	//	double pressure = CAST(double, data);

		//snprintf(buffer, sizeof(buffer), "pres: %f\r\n", pressure);
		//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

		//if(sdCardAvailable) {
		//	f_puts(buffer, &datacsv);
		//}
	//}
	//if(pHeader->StdId == 3) {
	//	double temperature = CAST(double, data);

		//snprintf(buffer, sizeof(buffer), "temp: %f\r\n\r\n", temperature);
		//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

		//if(sdCardAvailable) {
		//	f_puts(buffer, &datacsv);
		//}
	//}
}
