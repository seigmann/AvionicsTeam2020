#include "../../Inc/States/InitialState.h"

void InitialState::onEnter() {
    // This happens once, before anything else
}

void InitialState::onUpdate() {
    // This happens every time statemachine.update() is called
}

void InitialState::onExit() {
    // This happens last, after everything else
}