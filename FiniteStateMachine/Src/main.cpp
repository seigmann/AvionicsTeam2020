#include "../Inc/FSM/statemachine.h"
#include "../Inc/States/InitialState.h"

int main(void) {
    // First, define a StateMachine<I> where I is the initial state
    auto statemachine = StateMachine<InitialState>();

    while(1) {
        // Update the statemachine in the main loop
        statemachine.update();
    }

    return 0;
}
