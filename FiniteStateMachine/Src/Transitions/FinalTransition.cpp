#include "../../Inc/Transitions/FinalTransition.h"

bool FinalTransition::isValid() {
    // Transition after transition has been checked X times
    return ++i >= x;
}