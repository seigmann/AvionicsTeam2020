#ifndef FSM_LIBRARY_ITERATIONTRANSITION_H
#define FSM_LIBRARY_ITERATIONTRANSITION_H


#include "../FSM/transition.h"
#include "../States/FinalState.h"

// Transition to FinalState after the active state has been updated X times
class FinalTransition : public TransitionTo<FinalState> {
private:
    const int x = 10;
    int i = 0;

public:
    bool isValid() override;
};


#endif //FSM_LIBRARY_ITERATIONTRANSITION_H
