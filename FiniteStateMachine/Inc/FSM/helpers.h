#ifndef PROPULSE_FSM_HELPERS_H
#define PROPULSE_FSM_HELPERS_H


// Count number of transitions (state pointers) by counting arguments passed to TRANSITIONS(...)
// Stolen from: https://stackoverflow.com/questions/11317474/macro-to-count-number-of-arguments

#define ARG_N_LIST(_1,  _2,  _3,  _4,  _5,  _6,  _7,  _8,  _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, N, ...) N
#define ARG_N_SEQUENCE()    62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40,39, 38, 37, 36, 35, 34, 33, 32, 31, 30,29, 28, 27, 26, 25, 24, 23, 22, 21, 20,19, 18, 17, 16, 15, 14, 13, 12, 11, 10,9,  8,  7,  6,  5,  4,  3,  2,  1,  0
#define N_ARG(...)          ARG_N_LIST(__VA_ARGS__)
#define COUNT_OF(...)       N_ARG(_, ##__VA_ARGS__, ARG_N_SEQUENCE())


/** \def TRANSITIONS
 *	\breif Define what transitions are valid for this state
 */
#define TRANSITIONS(l...) public:					    \
	Transition* transitions[COUNT_OF(l)] = {l};		    \
	bool getValidTransition(State* &next) override {	\
		for(int i = 0; i < COUNT_OF(l); i++){		    \
			if(transitions[i]->isValid()){				\
				next = transitions[i]->getNextState();	\
				return true;							\
			}											\
		}												\
		return false;									\
	}

/** \def TRANSITION
 *	\breif Define what single transition is valid for this state
 */
#define TRANSITION(l) public:					    	\
	Transition* transition = l;		    		        \
	bool getValidTransition(State* &next) override {	\
		return transition->isValid();					\
	}

/** \def CLEANUP_TRANSITIONS
 *  \breif Frees the memory used by transitions, defined by TRANSITIONS
 */
#define CLEANUP_TRANSITIONS()								\
	for(int i = 0; i < COUNT_OF(transitions); i++) {      	\
		delete transitions[i];								\
	}

/** \def CLEANUP_TRANSITION
 *  \breif Frees the memory used by the states transition
 */
#define CLEANUP_TRANSITION() delete transition;

#endif //PROPULSE_FSM_HELPERS_H
