#ifndef PROPULSE_FSM_TRANSITION_H
#define PROPULSE_FSM_TRANSITION_H


#include "state.h"

/** TRANSITION
 * @breif Defines the conditions for going to a given state
 */

class Transition {
public:
    /// Must have a virtual destructor
    virtual ~Transition() {}

    virtual bool isValid() = 0;
    virtual State* getNextState() = 0;
};

/** TRANSITION TO
 * @breif Defines the conditions for going to the given state S
 * @tparam The next active state, given the conditions of this transition is valid
 */

template <typename S>
class TransitionTo : public Transition {
public:
    /// Return a new instance of the template-defined state S
    virtual State* getNextState() override {
        return new S();
    };
};


#endif //PROPULSE_FSM_TRANSITION_H
