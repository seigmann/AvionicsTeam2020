#ifndef FSM_LIBRARY_INITIALSTATE_H
#define FSM_LIBRARY_INITIALSTATE_H


#include "../FSM/state.h"
#include "../Transitions/FinalTransition.h"

class InitialState : public State {
    // If the state has a single transition, use TRANSITION()
    TRANSITION(new FinalTransition())

    // If the state has multiple transitions, use TRANSITIONS()
    // TRANSITIONS(new FinalTransition())


private:
    ~InitialState() {
        // If the state has a single transition, use CLEANUP_TRANSITION() in the destructor to prevent memory leaks
        CLEANUP_TRANSITION()

        // If the state has multiple transition, use CLEANUP_TRANSITIONS() in the destructor to prevent memory leaks
        // CLEANUP_TRANSITIONS()
    };

public:
    // Callbacks
    void onEnter() override;
    void onUpdate() override;
    void onExit() override;
};


#endif //FSM_LIBRARY_INITIALSTATE_H
