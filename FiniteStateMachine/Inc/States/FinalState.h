#ifndef FSM_LIBRARY_FINALSTATE_H
#define FSM_LIBRARY_FINALSTATE_H


// A state does not require any transitions, but the state machine will never leave this state
class FinalState : public State {
public:
    // Callbacks - Only include the callbacks you need
    void onEnter() override;
    void onUpdate() override;
    void onExit() override;
};


#endif //FSM_LIBRARY_FINALSTATE_H
