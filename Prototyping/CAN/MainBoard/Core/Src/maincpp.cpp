/*
 * maincpp.cpp
 *
 *  Created on: Feb 15, 2020
 *      Author: jonas
 */
 
 
/*
 General Info:
   http://www.bittiming.can-wiki.info/ was used to calculate the CAN timings
 

 IMPORTANT:
 	 After regenerating code set
 	 #define  USE_HAL_CAN_REGISTER_CALLBACKS         1U // CAN register callback disabled
 	 in stm32f1xx_hal_conf.h:136
 */

#include "main.h"
#include "fatfs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


extern CAN_HandleTypeDef hcan;
extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;


#define BUF_LENGTH 512
char buffer[BUF_LENGTH];

bool sdCardAvailable;

FATFS fs;  // file system
FIL datacsv; //, logtxt;  // file
FRESULT fresult;  // to store the result
UINT br, bw;   // file read/write count
/* capacity related variables */
FATFS *pfs;
DWORD fre_clust;
uint32_t total, free_space;

void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data);

void alloff();
bool InitSDCard();

extern "C" void maincpp(void) {
	sdCardAvailable = InitSDCard();

	// CAN Callbacks are configured in MX_CAN_Init()

	while(1) {
		if(sdCardAvailable) {
			f_sync(&datacsv);
		}
		HAL_Delay(1000);
	}
}


// Casts a pointer to an array d to type x
#define CAST(x, d) (*((x*) d))

void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data) {
	snprintf(buffer, sizeof(buffer), "%d %x %x\r\n", (int) pHeader->DLC, *((unsigned int*) (data+4)), *((unsigned int*) data));
	//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

	if(pHeader->StdId == 1) {
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);

		if(sdCardAvailable) {
			f_puts("click\n", &datacsv);
		}
	}
	if(pHeader->StdId == 2) {
		double pressure = CAST(double, data);

		snprintf(buffer, sizeof(buffer), "pres: %f\r\n", pressure);
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

		if(sdCardAvailable) {
			f_puts(buffer, &datacsv);
		}
	}
	if(pHeader->StdId == 3) {
		double temperature = CAST(double, data);

		snprintf(buffer, sizeof(buffer), "temp: %f\r\n\r\n", temperature);
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

		if(sdCardAvailable) {
			f_puts(buffer, &datacsv);
		}
	}

}

extern "C" void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
	snprintf(buffer, sizeof(buffer), "HAL_CAN_TxMailbox0CompleteCallback\r\n");
	HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
}

extern "C" void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &pHeader, data);
	CAN_Message_Received(&pHeader, data);
}

extern "C" void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &pHeader, data);
	CAN_Message_Received(&pHeader, data);
}


void alloff() {
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_9, GPIO_PIN_SET);
}


bool InitSDCard() {
	bool sdCardAvailable = false;

	/* Mount SD Card */
	fresult = f_mount(&fs, "", 0);
	if (fresult != FR_OK){
		//Send_Uart ((char*)"error in mounting SD CARD...\r\n");
		sdCardAvailable = false;
	}
	else {
		//Send_Uart((char*)"SD CARD mounted successfully...\r\n");
		sdCardAvailable = true;
	}

	if(sdCardAvailable) {
		/* Check free space */
		f_getfree("", &fre_clust, &pfs);

		total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
		if(total == 0)
			return false;

		//sprintf (buffer, "SD CARD Total Size: \t%lu\r\n",total);
		//Send_Uart(buffer);

		memset(buffer, 0, BUF_LENGTH);

		free_space = (uint32_t)(fre_clust * pfs->csize * 0.5);
		sprintf (buffer, "SD CARD Free Space: \t%lu\r\n",free_space);
		//Send_Uart(buffer);


		int file_number = 0;
		char file_name[20];
		do
		{
			file_number++;
			snprintf(file_name, sizeof(file_name) ,"data%02d.txt", file_number);
		} while(f_stat(file_name,NULL)==FR_OK);

		/* Open file to write/ create a file if it doesn't exist */
		fresult = f_open(&datacsv, file_name, FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
		f_puts("Hello World\n", &datacsv);

		f_sync(&datacsv);

		//fresult = f_open(&logtxt, "log.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
		//f_puts("SD card mounted and working\n", &logtxt);
	}

	return sdCardAvailable;
}
