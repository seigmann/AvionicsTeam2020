/*
 * maincpp.cpp
 *
 *  Created on: Feb 22, 2020
 *      Author: jonas
 */

#include "main.h"
#include "BMI088.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;

Bmi088 bmi(hspi1, GPIO_PIN_15, GPIO_PIN_3);

// global buffer
#define BUF_LENGTH 512
char buffer[BUF_LENGTH];

void SendUart (char *string);
void InitBMI088();

extern "C" void maincpp() {
	InitBMI088();

	while(1) {

	}
}



/* to send the data to the uart */
void SendUart (char *string) {
	HAL_UART_Transmit(&huart1, (uint8_t *) string, strlen (string), 2000);  // transmit in blocking mode
}

void InitBMI088() {
	int status = bmi.begin();
	if (status < 0) {
		snprintf(buffer, sizeof(buffer), "IMU Initialization Error %d\r\n", status);
		SendUart(buffer);
		while (1) {}
	}
	/* set the ranges */
	status = bmi.setRange(Bmi088::ACCEL_RANGE_24G, Bmi088::GYRO_RANGE_2000DPS);
	if (status < 0) {
		snprintf(buffer, sizeof(buffer), "Failed to set ranges %d\r\n", status);
		SendUart(buffer);
		while (1) {}
	}
	/* set the output data rate */
	status = bmi.setOdr(Bmi088::ODR_400HZ);
	if (status < 0) {
		snprintf(buffer, sizeof(buffer), "Failed to set ODR %d\r\n", status);
		SendUart(buffer);
		while (1) {}
	}
}
