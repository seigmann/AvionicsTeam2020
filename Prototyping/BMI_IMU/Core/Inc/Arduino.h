/*
 * Arduino.h
 *
 *  Created on: Feb 22, 2020
 *      Author: jonas
 */

#ifndef INC_ARDUINO_H_
#define INC_ARDUINO_H_


#include "stm32f1xx_hal.h"

#define M_PI 3.14159265359
#define HIGH GPIO_PIN_SET
#define LOW GPIO_PIN_RESET


#endif /* INC_ARDUINO_H_ */
