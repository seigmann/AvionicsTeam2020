/*
 * maincpp.cpp
 *
 *  Created on: Feb 11, 2020
 *      Author: Jonas Juffinger
 */



#include "main.h"
#include <ad7124.h>
#include <stdio.h>

using namespace Ad7124;

extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1;


Ad7124Chip adc;

const double Gain = 16;
const double Rf = 5.11E3;
const long Zero = 1L << 23;
const long FullScale = 1L << 24;


char string[120];


void Send_Uart (char *string)
{
	HAL_UART_Transmit(&huart1, (uint8_t *) string, strlen (string), 2000);  // transmit in blocking mode
}


extern "C" void maincpp(void) {

	Send_Uart((char*)"Hello World\r\n");


	Ad7124Driver driver;
	driver.init(GPIO_PIN_6, &hspi1);
	uint8_t wrBuf[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	driver.write(wrBuf, sizeof(wrBuf));


  // Initializes the AD7124 device, it has to be on PORTB (or change ad7124-driver.cpp)
  adc.begin (GPIO_PIN_6, &hspi1);

  // Setting the configuration 0:
  // - use of the REFIN1(+)/REFIN1(-) reference
  // - gain of 16 for a bipolar measurement
  // - digital filter Sync4 FS=384
  adc.setConfig (0, RefIn1, Pga16, true);
  adc.setConfigFilter (0, Sinc4Filter, 384);

  // Setting channel 0 with config 0 using pins AIN2(+)/AIN3(-)
  adc.setChannel (0, 0, AIN2Input, AIN3Input);

  // Configuring ADC in Full Power Mode (Fastest)
  int ret = adc.setAdcControl (StandbyMode, FullPower, true);
  if (ret < 0) {

  	Send_Uart ((char*)"Unable to setting up ADC");
  }


	Send_Uart((char*)"Init finished\r\n");


	while(1) {
	  long value;

	  // detect an open wire circuit (no RTD connected) or a short-circuit
	  adc.setConfig (0, RefInternal, Pga1, true, Burnout4uA);
	  value = adc.read (0);

	  if (value >= (FullScale - 10))  {

	    // A near full-scale reading can mean that the front-end sensor is open circuit.
	    adc.setConfig (0, RefInternal, Pga1, true, BurnoutOff);
	    Send_Uart ((char*)"OPENED");
	  }
	  else {

	    // Setting the configuration 0 for measuring
	    adc.setConfig (0, RefIn1, Pga16, true);
	    // Program the excitation currents to 500 μA and output the currents on the AIN0
	    adc.setCurrentSource (0, IoutCh0, Current500uA);

	    // Measuring on Channel 0 in Single Conversion Mode
	    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_SET);
	    value = adc.read (0);
	    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

	    // Program the excitation currents to Off
	    adc.setCurrentSource (0, IoutCh0, CurrentOff);

	    if (value >= 0) {
	      float r, t = 0;

	      // See Equation (1), p.4 of CN-0381
	      r = ((value - Zero) * Rf) / (Zero * Gain);

	      // See Equation (2), p.4 of CN-0381
	      t = (r - 100.0) / 0.385;

	      // Print results
	      snprintf(string, sizeof(string), "%10.5f, %10.5f\r\n", t, r);
	      Send_Uart(string);
	    }
	    else {

	    	Send_Uart ((char*)"FAIL");
	    }
	  }
	}
}

