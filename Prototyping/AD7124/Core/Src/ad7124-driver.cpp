/**
 * Copyright © 2017-2018 epsilonRT. All rights reserved.
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * <http://www.cecill.info>.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * @file
 * @brief
 */
/* ========================================================================== */
#include "ad7124-driver.h"

/* private functions ======================================================== */
// -----------------------------------------------------------------------------
static inline void
setSS (int ss_pin) {

	HAL_GPIO_WritePin(GPIOB, (uint16_t) ss_pin, GPIO_PIN_RESET); /* SS = 0 -> validé */
}

// -----------------------------------------------------------------------------
static inline void
clearSS (int ss_pin) {

	HAL_GPIO_WritePin(GPIOB, (uint16_t) ss_pin, GPIO_PIN_SET); /* SS = 0 -> validé */
}

// ---------------------------------------------------------------------------
static inline void
initSS (int ss_pin) {

  clearSS (ss_pin);
}

/* internal public functions ================================================ */
// -----------------------------------------------------------------------------
bool
Ad7124Driver::init (uint8_t slaveDeviceId, SPI_HandleTypeDef *_hspi) {

  id = slaveDeviceId;
  hspi = _hspi;
  initSS (id);
  return true;
}

// -----------------------------------------------------------------------------
int
Ad7124Driver::read (uint8_t* data, uint8_t len) {
	uint8_t read_data[len];
  setSS(id);
  HAL_SPI_TransmitReceive(hspi, data, read_data, len, 100);
  clearSS(id);
  memcpy(data, read_data, len);
  return len;
}

// -----------------------------------------------------------------------------
int
Ad7124Driver::write (uint8_t * data, uint16_t len) {
  setSS(id);
  HAL_SPI_Transmit(hspi, data, len, 100);
  clearSS(id);
  return len;
}

// -----------------------------------------------------------------------------
int
Ad7124Driver::delay (unsigned long ms) {

  HAL_Delay(ms);
  return 0;
}

/* ========================================================================== */
