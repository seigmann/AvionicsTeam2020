/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : DrougeState.h (State)
 * - Date       : 2. mar. 2020
 * - Description: Rocket has reached apogee and deployed the drouge chute.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_DROUGESTATE_H__
#define __INC_STATES_DROUGESTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"
#include "Transitions/LowAltitudeDetectedTransition.h"

// State
//-------------------------------------
class DrougeState : public State {
	TRANSITION_TO(LowAltitudeDetectedTransition)

public:
	~DrougeState();

public:
	/// Deploy drouge chute
	void onEnter() override;
};


#endif /* __INC_STATES_DROUGESTATE_H__ */
