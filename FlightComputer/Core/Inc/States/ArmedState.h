/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : ArmedState.h (State)
 * - Date	 	: 2. mar. 2020
 * - Description: Rocket is armed, only a single action is required to launch the rocket.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_ARMEDSTATE_H__
#define __INC_STATES_ARMEDSTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"
#include "Transitions/AbortTransition.h"
#include "Transitions/LaunchDetectedTransition.h"

// State
//-------------------------------------
class ArmedState : public State {
	TRANSITIONS(
			new LaunchDetectedTransition(),
			new AbortTransition()
	)

public:
	~ArmedState();

public:
	/// Activate secondary systems
	void onEnter() override;
};


#endif /* __INC_STATES_ARMEDSTATE_H__ */
