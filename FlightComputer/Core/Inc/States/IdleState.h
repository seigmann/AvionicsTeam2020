/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : IdleState.h
 * - Date 		: 29. feb. 2020
 * - Description: Rocket is idling after powering on primary systems.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_IDLESTATE_H__
#define __INC_STATES_IDLESTATE_H__


// Includes
//-------------------------------------
#include "FSM/state.h"
#include "Transitions/ArmTransition.h"
#include "Transitions/AbortTransition.h"

// State
//-------------------------------------
class IdleState : public State {
	TRANSITIONS(
		new ArmTransition(),
		new AbortTransition()
	)

public:
	~IdleState();

public:
	/// Activate primary systems
	void onEnter() override;
};


#endif /* __INC_STATES_IDLESTATE_H__ */
