/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : MainChuteDeployState.h (State)
 * - Date       : 7. mar. 2020
 * - Description: Rocket deploys the main chute at below 470m.
 * - 
 * -------------------------------------*/
#ifndef __INC_STATES_MAINCHUTEDEPLOYSTATE_H__
#define __INC_STATES_MAINCHUTEDEPLOYSTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"

// State
//-------------------------------------
class MainChuteDeployState : public State {
	// TRANSITION_TO()

public:
	~MainChuteDeployState();

public:
	/// Deploy main chute
	void onEnter() override;
};


#endif /* __INC_STATES_MAINCHUTEDEPLOYSTATE_H__ */
