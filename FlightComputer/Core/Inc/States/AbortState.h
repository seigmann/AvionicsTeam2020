/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : AbortState.h (State)
 * - Date       : 2. mar. 2020
 * - Description: Rocket launch is aborted, but can be reset.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_ABORTSTATE_H__
#define __INC_STATES_ABORTSTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"
#include "Transitions/ResetLaunchTransition.h"

// State
//-------------------------------------
class AbortState : public State {
	TRANSITION_TO(ResetLaunchTransition)

public:
	~AbortState();

public:
	/// Deactivate non essential systems
	/// Primary systems will be reactivated if mission is reset
	void onEnter() override;
};


#endif /* __INC_STATES_ABORTSTATE_H__ */
