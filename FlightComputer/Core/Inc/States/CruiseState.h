/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : CruiseState.h (State)
 * - Date       : 2. mar. 2020
 * - Description: Rocket is climbing.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_CRUISESTATE_H__
#define __INC_STATES_CRUISESTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"
#include "Transitions/ApogeeDetectedTransition.h"

// State
//-------------------------------------
class CruiseState : public State {
	TRANSITION_TO(ApogeeDetectedTransition)

public:
	~CruiseState();

	// void onEnter() override;
	// void onUpdate() override;
	// void onExit() override;
};


#endif /* __INC_STATES_CRUISESTATE_H__ */
