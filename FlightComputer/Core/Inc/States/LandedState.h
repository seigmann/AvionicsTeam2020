/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : LandedState.h (State)
 * - Date       : 7. mar. 2020
 * - Description: Rocket is stationary. Cut power to all systems that are not essential for the physical recovery of the rocket.
 * -
 * -------------------------------------*/
#ifndef __INC_STATES_LANDEDSTATE_H__
#define __INC_STATES_LANDEDSTATE_H__

// Includes
//---------------------------------
#include "FSM/state.h"

// State
//-------------------------------------
class LandedState : public State {
public:
	/// Disable non-essential systems
	void onEnter() override;
};


#endif /* __INC_STATES_LANDEDSTATE_H__ */
