/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : sdCard.h              
 * - Date       : 29. feb. 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

#ifndef __INC_SDCARD_H__
#define __INC_SDCARD_H__

// Includes
//---------------------------------
#include <stdbool.h>

// Defines
//---------------------------------


// Prototype Declarations
//---------------------------------
bool sdCard_init();

#endif /* INC_SDCARD_H_ */
