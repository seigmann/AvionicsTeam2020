/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : ArmTransition.h (Transition)
 * - Transition To : ArmedState.h
 * - Date          : 2. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_ARMTRANSITION_H__
#define __INC_TRANSITIONS_ARMTRANSITION_H__

// Includes
//---------------------------------
#include "FSM/transition.h"
#include "States/ArmedState.h"

// State
//-------------------------------------
class ArmTransition : public TransitionTo<ArmedState> {
public:
	/// Valid when armed flag is active
	/// Armed flag is toggled via radio communication
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_ARMTRANSITION_H__ */
