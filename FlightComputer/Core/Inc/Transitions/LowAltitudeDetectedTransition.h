/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : LowAltitudeDetectedTransition.h (Transition)
 * - Transition To : MainChuteDeployState.h
 * - Date          : 7. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_LOWALTITUDEDETECTEDTRANSITION_H__
#define __INC_TRANSITIONS_LOWALTITUDEDETECTEDTRANSITION_H__

// Includes
//---------------------------------
#include "FSM/transition.h"
#include "States/MainChuteDeployState.h"

// Transition
//-------------------------------------
class LowAltitudeDetectedTransition : public TransitionTo<MainChuteDeployState> {
public:
	/// Valid when rocket is below 470m
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_LOWALTITUDEDETECTEDTRANSITION_H__ */
