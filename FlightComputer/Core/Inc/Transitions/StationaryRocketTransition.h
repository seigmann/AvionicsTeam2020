/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : StationaryRocketTransition.h (Transition)
 * - Transition To : LandedState.h
 * - Date          : 7. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_STATIONARYROCKETTRANSITION_H__
#define __INC_TRANSITIONS_STATIONARYROCKETTRANSITION_H__

// Includes
//---------------------------------
#include "FSM/transition.h"
#include "States/LandedState.h"

// Transition
//-------------------------------------
class StationaryRocketTransition : public TransitionTo<LandedState> {
public:
	/// Valid when rocket is stationary
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_STATIONARYROCKETTRANSITION_H__ */
