/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : LaunchDetectedTransition.h (Transition)
 * - Transition To : CruiseState.h
 * - Date          : 2. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_LAUNCHDETECTEDTRANSITION_H__
#define __INC_TRANSITIONS_LAUNCHDETECTEDTRANSITION_H__

// Includes
//---------------------------------
#include "FSM/transition.h"
#include "States/CruiseState.h"

// State
//-------------------------------------
class LaunchDetectedTransition : public TransitionTo<CruiseState> {
public:
	/// Valid when acceleration is great enough
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_LAUNCHDETECTEDTRANSITION_H__ */
