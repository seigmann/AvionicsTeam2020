/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : ResetLaunchState.h (Transition)
 * - Transition To : IdleState.h
 * - Date          : 2. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_RESETLAUNCHSTATE_H__
#define __INC_TRANSITIONS_RESETLAUNCHSTATE_H__

// Includes
//---------------------------------
#include "FSM/transition.h"

// Transition
//-------------------------------------
class ResetLaunchTransition : public Transition {
public:
	/// Avoid circular dependency by not using TransitionTo<> helper
	State* getNextState() override;

	/// Valid if abort flag is not set
	/// Abort flag is toggled from radio communication
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_RESETLAUNCHSTATE_H__ */
