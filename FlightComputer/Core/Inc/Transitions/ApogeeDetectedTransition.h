/*
 * ApogeeDetectedTransition.h
 *
 *  Created on: 2. mar. 2020
 *      Author: Imre Angelo
 */

#ifndef INC_TRANSITIONS_APOGEEDETECTEDTRANSITION_H_
#define INC_TRANSITIONS_APOGEEDETECTEDTRANSITION_H_

#include "FSM/transition.h"
#include "States/DrougeState.h"

class ApogeeDetectedTransition : public TransitionTo<DrougeState> {
public:
	bool isValid() override;
};

#endif /* INC_TRANSITIONS_APOGEEDETECTEDTRANSITION_H_ */
