/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename      : AbortTransition.h (Transition)
 * - Transition To : AbortState.h
 * - Date          : 2. mar. 2020
 * - Description   :
 * -
 * -------------------------------------*/
#ifndef __INC_TRANSITIONS_ABORTTRANSITION_H__
#define __INC_TRANSITIONS_ABORTTRANSITION_H__

// Includes
//---------------------------------
#include "FSM/transition.h"
#include "States/AbortState.h"

// Transition
//-------------------------------------
class AbortTransition : public TransitionTo<AbortState> {
public:
	/// Valid if abort flag is set
	/// Abort flag is toggled from radio communication
	bool isValid() override;
};


#endif /* __INC_TRANSITIONS_ABORTTRANSITION_H__ */
