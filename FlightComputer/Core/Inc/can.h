/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : can.h
 * - Date       : 29. feb. 2020
 * - Description: This is the set up of
 * the CAN bus and the interrupts for it.
 * -
 * -------------------------------------*/

#ifndef __CAN_H__
#define __CAN_H__

// Includes
//-------------------------------------
#include <stdbool.h>
#include "stm32f1xx_hal.h"


// Function Prototypes
//-------------------------------------
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan);
void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan);
void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan);
void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data);

#endif
