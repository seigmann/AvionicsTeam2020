/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : data.hpp              
 * - Date       : 2. mar. 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

#ifndef __INC_DATA_HPP__
#define __INC_DATA_HPP__

// Variables
//---------------------------------
uint8_t buffer[255] = {};

// Defines
//---------------------------------
enum Axis {X, Y, Z};

// Typedefs
//---------------------------------
struct Data {
	double accel [3];
	double angVel[3];
	double temperature;
	double pressure;
	float  longitude;
	float  latitude;
	float  altitude;
	float  speedOverGround;
	float  courseOverGround;
	float  magneticVariation;
};

struct LogData {
	double temperature1;
	double temperature2;
	double temperature3;
	double pressure1;
	double pressure2;
	double pressure3;
	uint16_t nrOfSattelites;
	double altitudePressure;
};


#endif /* INC_DATA_HPP_ */
