#ifndef TEMPLATED_MACROS_H
#define TEMPLATED_MACROS_H


// TODO: Count number of transitions (state pointers)
// Stolen from: https://stackoverflow.com/questions/11317474/macro-to-count-number-of-arguments

#define PP_ARG_N( \
          _1,  _2,  _3,  _4,  _5,  _6,  _7,  _8,  _9, _10, \
         _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, \
         _21, _22, _23, _24, _25, _26, _27, _28, _29, _30, \
         _31, _32, _33, _34, _35, _36, _37, _38, _39, _40, \
         _41, _42, _43, _44, _45, _46, _47, _48, _49, _50, \
         _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, \
         _61, _62, _63, N, ...) N

#define PP_RSEQ_N()                                        \
         62, 61, 60,                                       \
         59, 58, 57, 56, 55, 54, 53, 52, 51, 50,           \
         49, 48, 47, 46, 45, 44, 43, 42, 41, 40,           \
         39, 38, 37, 36, 35, 34, 33, 32, 31, 30,           \
         29, 28, 27, 26, 25, 24, 23, 22, 21, 20,           \
         19, 18, 17, 16, 15, 14, 13, 12, 11, 10,           \
          9,  8,  7,  6,  5,  4,  3,  2,  1,  0

#define PP_NARG_(...)    PP_ARG_N(__VA_ARGS__)
#define COUNT_OF(...)     PP_NARG_(_, ##__VA_ARGS__, PP_RSEQ_N())


/** \def TRANSITIONS
 *	\breif Define what transitions are valid for a given state.
 */
#define TRANSITIONS(l...) public:					    	\
	Transition* transitions[COUNT_OF(l)] = {l};		    	\
	bool getValidTransition(State* &next) override {		\
		for(int i = 0; i < COUNT_OF(l); i++){		    	\
			if(transitions[i]->isValid()){					\
				next = transitions[i]->getNextState();		\
				return true;								\
			}												\
		}													\
		return false;										\
	}														\
															\
	void terminateState() {									\
		for(int i = 0; i < COUNT_OF(transitions); i++) {    \
			delete transitions[i];							\
		}													\
	}

/** \def TRANSITION_TO
 *	\breif Define what single transition is valid for a given state.
 */
#define TRANSITION_TO(l) public:					    	\
	l* transition = new l();		    					\
	bool getValidTransition(State* &next) override {		\
		return transition->isValid();						\
	}														\
															\
	void terminateState() {									\
		delete transition;									\
	}


#endif //TEMPLATED_MACROS_H
