/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : can_defs.h              
 * - Date       : 2. mar. 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

#ifndef __INC_CAN_DEFS_H__
#define __INC_CAN_DEFS_H__

// Defines
//---------------------------------
enum class CAN_ID {
			STATUS = 0,
			ENTER_STATE,
			EXIT_STATE,

			FUSED_PRESSURE = 500,
			FUSED_TEMPERATURE,
			HIGH_ALTITUDE_PRESSURE,
			ACCELERATION_X,
			ACCELERATION_Y,
			ACCELERATION_Z,
			ANG_VELOCITY_X,
			ANG_VELOCITY_Y,
			ANG_VELOCITY_Z,
			LONGITUDE,
			LATITUDE,
			ALTITUDE,
			SPEED_OVER_GROUND,
			COURSE_OVER_GROUND,
			MAGNETIC_VARIATION,

			LOG_PRESSURE_1 = 1000,
			LOG_BARO_TEMP_1,
			LOG_PRESSURE_2,
			LOG_BARO_TEMP_2,
			LOG_PRESSURE_3,
			LOG_BARO_TEMP_3,
			GPS_STATUS
};

#endif /* INC_CAN_DEFS_H_ */
