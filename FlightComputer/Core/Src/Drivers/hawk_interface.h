//Timer defs
#define HAWK_TIMER 			&htim1
#define HAWK_CHANNEL 		TIM_CHANNEL_1
#define HAWK_INSTANCE 		htim1.Instance
#define HAWK_COMPARE		CCR1

//Servo Positions
#define HAWK_POSITION_0		220
#define HAWK_POSITION_180	80

//LED Definitions
#define LED0	GPIOA
#define	LED1	GPIOB
#define	LED2	GPIOB
#define	LED3	GPIOB

#define LED0_PIN	GPIO_PIN_0
#define	LED1_PIN	GPIO_PIN_7
#define	LED2_PIN	GPIO_PIN_8
#define	LED3_PIN	GPIO_PIN_9
