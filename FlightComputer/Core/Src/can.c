/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : can.c
 * - Date       : 29. feb. 2020
 * - Description: This is the set up of
 * the CAN bus and the interrupts for it.
 * -
 * -------------------------------------*/

// Includes
//-------------------------------------
#include "can.h"
#include "fatfs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Defines
//-------------------------------------
#define BUF_LENGTH 512
#define CAST(x, d) (*((x*) d)) // Casts a pointer to an array d to type x

// Variables
//-------------------------------------
char buffer[BUF_LENGTH];
extern CAN_HandleTypeDef hcan;
extern UART_HandleTypeDef huart1;

// Function Definitions
//-------------------------------------


void CAN_Message_Received(CAN_RxHeaderTypeDef *pHeader, uint8_t *data) {
	snprintf(buffer, sizeof(buffer), "%d %x %x\r\n", (int) pHeader->DLC, *((unsigned int*) (data+4)), *((unsigned int*) data));
	//HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);

	if(pHeader->StdId == 1) {
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
	}
	if(pHeader->StdId == 2) {
		double pressure = CAST(double, data);

		snprintf(buffer, sizeof(buffer), "pres: %f\r\n", pressure);
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
	}
	if(pHeader->StdId == 3) {
		double temperature = CAST(double, data);

		snprintf(buffer, sizeof(buffer), "temp: %f\r\n\r\n", temperature);
		HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
	}

}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
	snprintf(buffer, sizeof(buffer), "HAL_CAN_TxMailbox0CompleteCallback\r\n");
	HAL_UART_Transmit(&huart1, (uint8_t*) buffer, strlen(buffer), 100);
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &pHeader, data);
	CAN_Message_Received(&pHeader, data);
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
	uint8_t data[10];
	CAN_RxHeaderTypeDef pHeader;
	HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO1, &pHeader, data);
	CAN_Message_Received(&pHeader, data);
}
