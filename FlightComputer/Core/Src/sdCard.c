/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : sdCard.c              
 * - Date       : 29. feb. 2020                       
 * - Description:           
 * -           
 * -------------------------------------*/

// Includes
//---------------------------------
#include "sdCard.h" 
#include "fatfs.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include "data.hpp"

// Variables
//---------------------------------
FATFS fs;  // file system
FIL datacsv; //, logtxt;  // file
FRESULT fresult;  // to store the result
UINT br, bw;   // file read/write count
/* capacity related variables */
FATFS *pfs;
DWORD fre_clust;
uint32_t total, free_space;

// Prototype Definitions
//---------------------------------
bool sdCard_init() {
	bool sdCardAvailable = false;

	/* Mount SD Card */
	fresult = f_mount(&fs, "", 0);
	if (fresult != FR_OK){
		//Send_Uart ((char*)"error in mounting SD CARD...\r\n");
		sdCardAvailable = false;
	}
	else {
		//Send_Uart((char*)"SD CARD mounted successfully...\r\n");
		sdCardAvailable = true;
	}

	if(sdCardAvailable) {
		/* Check free space */
		f_getfree("", &fre_clust, &pfs);

		total = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
		if(total == 0)
			return false;

		//sprintf (buffer, "SD CARD Total Size: \t%lu\r\n",total);
		//Send_Uart(buffer);

		memset(buffer, 0, sizeof(buffer));

		free_space = (uint32_t)(fre_clust * pfs->csize * 0.5);
		sprintf (buffer, "SD CARD Free Space: \t%lu\r\n",free_space);
		//Send_Uart(buffer);


		int file_number = 0;
		char file_name[20];
		do
		{
			file_number++;
			snprintf(file_name, sizeof(file_name) ,"data%02d.txt", file_number);
		} while(f_stat(file_name,NULL)==FR_OK);

		/* Open file to write/ create a file if it doesn't exist */
		fresult = f_open(&datacsv, file_name, FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
		f_puts("Hello World\n", &datacsv);

		f_sync(&datacsv);

		//fresult = f_open(&logtxt, "log.txt", FA_OPEN_ALWAYS | FA_READ | FA_WRITE);
		//f_puts("SD card mounted and working\n", &logtxt);
	}

	return sdCardAvailable;
}
