/* --------------------------
 * - PropulseNTNU Team 2020 -
 * --------------------------
 * - Filename   : radio.c              
 * - Date       : 29. feb. 2020                       
 * - Description: Sends and recieves data
 * - from the radio via UART.
 * -------------------------------------*/

// Includes
//---------------------------------
#include "radio.h" 


// Defines
//---------------------------------


// Prototype Definitions
//---------------------------------

