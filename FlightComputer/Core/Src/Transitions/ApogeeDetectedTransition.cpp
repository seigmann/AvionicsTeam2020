#include <Transitions/ApogeeDetectedTransition.h>
#include "./Drivers/hawk_interface.h"

bool ApogeeDetectedTransition::isValid() {
	// return true når apogee detected
	if ((apogee->recoveryData[MAX_ALTITUDE] - apogee->recoveryData[AVERAGE_ALTITUDE]) > APOGEE_ALTITUDE_MARGIN) {
		apogee->recoveryData[TIMESTAMP_APOGEE] = data[TIMESTAMP];
		return true;
	} else {
		return false;
	}
}
