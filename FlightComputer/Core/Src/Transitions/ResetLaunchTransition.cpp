#include <Transitions/ResetLaunchTransition.h>
#include <States/IdleState.h>

State* ResetLaunchTransition::getNextState() {
	return new IdleState();
}

bool ResetLaunchTransition::isValid() {
	// return true if abort flag is inactive, toggled via radio comms.
	return false;
}
