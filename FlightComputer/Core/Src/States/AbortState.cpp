#include <States/AbortState.h>

AbortState::~AbortState() {
	terminateState();
}

void AbortState::onEnter() {
	// turn off secondary systems to conserve power
}
