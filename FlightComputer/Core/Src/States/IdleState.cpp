#include <States/IdleState.h>

IdleState::~IdleState() {
	terminateState();
}

void IdleState::onEnter() {
	// Activate primary systems (gps, sd card etc.)
}
