#to send to UART: send to radios IP and port 20000
import os
from socket import *
from msvcrt import *

host = "10.19.37.136" # Rocket radio IP 
#host = "10.19.36.213" # other radio IP 
port = 20000    # for UART

addr = (host, port)
sock = socket(AF_INET, SOCK_DGRAM)


def setup():
    sock.sendto("9999".encode(), addr)

def sendMsg(msg):
    sock.sendto(msg.encode(), addr)
    #print("Sent message:", msg)    #debug

def recvMsg():
    msg, address = sock.recvfrom(4096)
    msg = msg.decode().strip()
    print(msg)
    #sendMsg("0")
    if (len(msg) > 0):
        return msg
    return '='    


#main
setup()

while True:
    if (kbhit()):
        key = getch().decode()
        if (key == 'l'):
            print(key)
            #print("Sending message")
            sendMsg("Launching rocket!")
            
        elif (key == 'x'):
            sendMsg(key)
            break

    #else:
    msg = recvMsg()
    # if (msg != '='):
    #     print(msg) 

    
        
sock.close()
print("Exiting program")
os._exit(0)
