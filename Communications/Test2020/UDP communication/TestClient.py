#to send to UART send to radios IP and port 20000
import os
from socket import *
#host = "10.19.127.14" # set to IP address of target device
#port = 12000

host = "10.19.37.136" # radio IP for message to Filling Station UART
#host = "10.19.36.213" # radio IP for message to Rocket UART
port = 20000    # for UART

addr = (host, port)
UDPSock = socket(AF_INET, SOCK_DGRAM)

while True:
    data = input("Enter message to send or type 'exit': ")
    #data = data.encode()
    UDPSock.sendto(data.encode(), addr)
    print(data)
    if data == "exit":
        break
UDPSock.close()
os._exit(0)
