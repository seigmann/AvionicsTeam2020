import time
from socket import *

# Get the server hostname and port as command line arguments                    
host = "10.19.127.14"   #IP of server computer
port = 12000
timeout = 1 # in seconds
 
# Create UDP client socket	
clientSocket = socket(AF_INET, SOCK_DGRAM)

# Set socket timeout as 1 second
socketTimeout = 1

# Sequence number of the ping message
ptime = 0  

# Ping for 10 times
while ptime < 10: 
    ptime += 1
    startTime = time.asctime() #Time in nice format
    	
    data = "ping " + str(ptime) + " " + str(startTime)
    data = data.encode()

    try:
        clientSocket.settimeout(socketTimeout)
        
	# Record the "sent time"
        sendTime = time.time() #time from system

	# Send the UDP packet with the ping message
        clientSocket.sendto(data, (host, port))

	# Receive the server response
        rcv, address = clientSocket.recvfrom(1024)

	# Record the "received time"
        rcvTime = time.time() #time from system

	# Display the server response as an output
        print(rcv.decode())

	# Round trip time is the difference between sent and received time
        rtt = rcvTime - sendTime
        print("RTT: " + str(rtt))
        
    except Exception as e:
        # print(e)
        # Server does not respond
	# Assume the packet is lost
        print("Request timed out.")
        continue

# Close the client socket
clientSocket.close()

