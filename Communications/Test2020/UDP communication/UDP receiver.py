from socket import *

UDP_IP = ""
UDP_PORT = 20000
sock = socket(AF_INET, SOCK_DGRAM) #Internet, UDP
sock.bind((UDP_IP, UDP_PORT))

print("Ready to receive packets!")

while True:
    data, addr = sock.recvfrom(1024) #Buffer size is 1024 bytes
    print("received message:", data)