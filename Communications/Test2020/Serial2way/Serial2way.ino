#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2,3); //Rx, Tx
uint32_t baudRate = 115200;   //uint32_t and unsigned long is the same (kinda)
unsigned long time;

void setup() {
  Serial.begin(baudRate);
  mySerial.begin(baudRate);
}

void loop() {
  if(Serial.available() > 0) {
    String input = Serial.readString();
    mySerial.println(input);
    Serial.println(input);
  }
  
   if(mySerial.available() > 0) {
    String input = mySerial.readString();
    Serial.println(input);
   }
   
}
