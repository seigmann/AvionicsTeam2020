#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2,3); //Rx, Tx
uint32_t baudRate = 115200;   //uint32_t and unsigned long is the same (kinda)
unsigned long time;

void setup() {
  Serial.begin(baudRate);
  mySerial.begin(baudRate);
}

void loop() {
   for (int i = 0; i < 10; i++) {
    time = millis();
    Serial.print(time);
    Serial.print(",");
    Serial.print(char(i+97));
    Serial.print(",");
    Serial.println(i);
    
    mySerial.print(time);
    mySerial.print(",");
    mySerial.print(char(i+97));
    mySerial.print(",");
    mySerial.println(i);
    delay(900);
   }
}
