#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); //Rx, Tx, rememeber: Tx to Rx and Rx to Tx
uint32_t baudRate = 115200;   //uint32_t and unsigned long is the same (kinda)
unsigned long time;
bool ended = false;
char input;
String msgIn = "";
uint32_t i = 0;


void setup() {
  Serial.begin(baudRate);
  mySerial.begin(baudRate);
}

void loop() {
  //Automated sending (comment out if you don't want to use it)
  if (Serial.available() <= 0) {
    mySerial.print(i);
    Serial.println(i);
    i++;
    delay(1000);
  }
  
 //Send serially from Arduino
  while (Serial.available() > 0) {
    char input = Serial.read();
    msgIn += input;
    mySerial.print(input);
    //Serial.print(input);
    ended = true;
  }

  //Receive serially to Arduino
  while (mySerial.available() > 0) {
    char input = mySerial.read();
    if (input < -37 && input > -128) {
      input += 192;
    }
    else if (input < 0) {
      input += 128;
    }
    msgIn += input;
    //Serial.print(input);
    ended = true;
  }


  //ended = True when the entire message is received
  //Sending the entire message serially
  if (ended) {
    ended = false;
    msgIn.trim();
    if (msgIn != "0") {
      Serial.println(msgIn);
    }

    if (msgIn == "9999") {
      Serial.println("Connection established");
      mySerial.println("Connection established");
    }
    msgIn = "";
  }

}
