#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); //Rx, Tx
uint32_t baudRate = 115200;   //uint32_t and unsigned long is the same (kinda)
unsigned long time;
bool ended = false;
char input;
String output = "";

int i = 0;

void setup() {
  Serial.begin(baudRate);
  mySerial.begin(baudRate);
}

void loop() {
  while (Serial.available() > 0) {
    char input = Serial.read();
    output += input;
    mySerial.print(input);
    //Serial.print(input);
    ended = true;
  }

  while (mySerial.available() > 0) {
    char input = mySerial.read();
    if (input < -37 && input > -128) {
      input += 192;
    }
    else if (input < 0) {
      input += 128;
    }
    output += input;
    //Serial.print(input);
    ended = true;
  }

  if (ended) {
    ended = false;
    output.trim();
    //Serial.println();
    Serial.println(output);
    if (output == "9999") {
      Serial.println("Connection established");
      mySerial.println("Connection established");
    }
    output = "";
  }

  if (Serial.available() <= 0) {
    output += i;
    mySerial.print(input);
    //Serial.print(input);
    ended = true;
    i++;
    delay(500);
  }

}
