#!/usr/bin/env python3

import serial
import csv

#Set these parameters right before executing program
#Press CTRL+C to stop the program
comport = '/dev/ttyUSB0'    #choose right COM-port!!!! Check device manager
filename = 'outputTest1.csv'    #name of file to save the data to
#what fields do we have?
field1 = 'Time'
field2 = 'X'
field3 = 'Y'
field4 = 'Z'
field5 = 'Pressure'
field6 = 'Temperature'
field7 = 'Latitude'
field8 = 'Longitude'
field9 = 'Height'
field10 = 'Velocity'
field11 = 'Angular'

fieldnames = [field1, field2,field3 , field4, field5, field6,
field7, field8, field9, field10, field11]


ser = serial.Serial(comport, baudrate = 115200, timeout = 1)
#baudrate = 115200, timeout is max wait time
#before it force checks the serial input

myDict = {}

try: #check if file already excists, append to it
    f = open(filename)
    with open(filename, 'a', newline='') as csvFile:
        csvWriter = csv.DictWriter(csvFile, fieldnames = fieldnames)
        csvWriter.writerow({field1: '---------', field2: '---------',
        field3: '---------', field4: '---------', field5: '---------',
        field6: '---------', field7: '---------',
        field8: '---------', field9: '---------',
        field10: '---------', field11: '---------'})
        # This is to see where the new recording started

        while True:
            serialLine = ser.readline()
            try:
              flightData = serialLine.decode('utf-8').strip().split(',')
              
              #reads data from the serial port and creates a list with values
              fdLen = len(flightData)
              if (fdLen > 1): #check if list is empty
                  for i in range(fdLen):
                      myDict[fieldnames[i]] = flightData[i]   #update dictionary
                  print(myDict)   # for debugging
                  csvWriter.writerow(myDict)  #write dictionary to csv file
              
            except:
              continue
    f.close()
except IOError: #if file doesn't excist already, create it
    with open(filename, 'w', newline='') as csvFile:
        csvWriter = csv.DictWriter(csvFile, fieldnames = fieldnames)
        csvWriter.writeheader()

        while True:
            flightData = ser.readline().decode('utf-8').strip().split(',')
            #reads data from the serial port and creates a list with values
            fdLen = len(flightData)
            if (fdLen > 1): #check if list is empty
                for i in range(fdLen):
                    myDict[fieldnames[i]] = flightData[i]   #update dictionary
                print(myDict)   # for debugging
                csvWriter.writerow(myDict)  #write dictionary to csv file

except KeyboardInterrupt:
    print('Interrupted by user. Data saved to file: %s' % filename)
    f.close()




