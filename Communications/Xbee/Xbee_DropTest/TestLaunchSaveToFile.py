import serial
import csv

#Set these parameters right before executing program
#Press CTRL+C to stop the program
comport = 'COM5'    #choose right COM-port!!!! Check device management
filename = 'outputTest1.csv'    #what file will you save the date to
fieldnames = ['Time', 'Char', 'Int'] #what fields do we have?

ser = serial.Serial(comport, baudrate = 115200, timeout = 1)
#baudrate = 115200, timeout is max wait time
#before it force checks the serial input

myDict = {}

try: #check if file already excists, append to it
    f = open(filename)
    with open(filename, 'a', newline='') as csvFile:
        csvWriter = csv.DictWriter(csvFile, fieldnames = fieldnames)
        csvWriter.writerow({'Time': '---------', 'Char': '---------',
        'Int': '---------'})

        while True:
            flightData = ser.readline().decode('utf-8').strip().split(',')
            #reads data from the serial port and creates a list with values
            fdLen = len(flightData)
            if (fdLen > 0): #check if list is empty
                for i in range(fdLen):
                    myDict[fieldnames[i]] = flightData[i]   #update dictionary
                print(myDict)
                csvWriter.writerow(myDict)  #write dictionary to csv file
    f.close()
except IOError: #if file doesn't excist already, create it
    with open(filename, 'w', newline='') as csvFile:
        csvWriter = csv.DictWriter(csvFile, fieldnames = fieldnames)
        csvWriter.writeheader()

        while True:
            flightData = ser.readline().decode('utf-8').strip().split(',')
            #reads data from the serial port and creates a list with values
            fdLen = len(flightData)
            if (fdLen > 0): #check if list is empty
                for i in range(fdLen):
                    myDict[fieldnames[i]] = flightData[i]   #update dictionary
                print(myDict)
                csvWriter.writerow(myDict)  #write dictionary to csv file

except KeyboardInterrupt:
    print('Interrupted by user. Data saved to file' + filename)




