#include <Arduino.h>
#include <SoftwareSerial.h>

SoftwareSerial mySerial(2, 3); //RX, TX
uint32_t baudRate = 115200;

void setup() {
  Serial.begin(baudRate);
  mySerial.begin(baudRate);
}

void loop() {
   
  if(Serial.available() > 0){//Read from serial monitor and send over HC-12
    String input = Serial.readString();
    mySerial.println(input);
    Serial.print(input);
    //delay(50);
  }
 
  if(mySerial.available() > 1){//Read from HC-12 and send to serial monitor
    String input = mySerial.readString();
    Serial.println(input);    
  }

//if(mySerial.available() > 1) {//Read from HC-12 and send to serial monitor
//    char input = mySerial.read();
//    Serial.print(input);
//  }
  //delay(5);
}
